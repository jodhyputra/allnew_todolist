package com.rajo;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {
    @Test
    public void addToDoListTest() {
        String expectedTodo = "1. Do Dishes [DONE] -> Home\n2. Learn Java [DONE] -> Learn\n3. Learn TDD [NOT DONE] -> Learn";

        ToDoList todolist = new ToDoList();

        ToDoList.insertToDoList(1, "Do Dishes", "DONE", "Home");
        ToDoList.insertToDoList(2, "Learn Java", "DONE", "Learn");
        ToDoList.insertToDoList(3, "Learn TDD", "NOT DONE", "Learn");

        String actual = todolist.getToDoList();

        assertEquals(expectedTodo, actual);
    }
    @Test
    public void deleteToDoListTest() {
        String expected = null;

        Task toDoList = new Task();

        ToDoList.deleteToDoList(toDoList);
        toDoList = null;

        assertEquals(expected, toDoList);
    }
}