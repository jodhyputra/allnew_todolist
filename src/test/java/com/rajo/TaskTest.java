package com.rajo;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void taskDataIsCorrectTest() {
        String expectedTask = "1. Do Dishes [DONE] -> school";

        Task task = new Task();

        task.insertTaskData(1, "Do Dishes", "DONE", "school");
        assertEquals(expectedTask, task.getTaskData());
    }

    @Test
    public void taskIsNullTest() {
        Task task = new Task();
        assertNull(task.getTaskData());
    }
}
