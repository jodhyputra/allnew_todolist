package com.rajo;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int inputMenu;
        int selectTask;
        Scanner myScan = new Scanner(System.in);

        do {
            DisplayMenu.showMenu();
            System.out.println("Input: ");
            inputMenu = myScan.nextInt();

            switch (inputMenu) {
                case 0:
                    System.out.println("Thanks for use our feature");
                    break;

                case 1:
                    DisplayMenu.formList();
                    break;

                case 2:
                    DisplayMenu.infoViewAllList();
                    break;

                case 3:
                    DisplayMenu.infoViewCategory();
                    break;

                case 4:
                    DisplayMenu.infoMarkDone();
                    selectTask = myScan.nextInt();
                    System.out.println(ToDoList.makeDoneToDoList(ToDoList.searchToDoList(selectTask)));
                    break;

                case 5:
                    DisplayMenu.infoDeleteList();
                    selectTask = myScan.nextInt();
                    System.out.println(ToDoList.deleteToDoList(ToDoList.searchToDoList(selectTask)));
                    break;

                default:
                    System.out.println("Not valid number");
                    break;
            }
            DisplayMenu.printSpacing(5);
        } while (inputMenu != 0);
    }
}