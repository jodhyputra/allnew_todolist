package com.rajo;

import java.util.ArrayList;

public class ToDoList {
    private static ArrayList<Task> todolist = new ArrayList<>();

    public static String deleteToDoList(Task toDoList) {
        String info;

        if (toDoList != null) {
            todolist.remove(toDoList);
            info = "Task deleted";
        } else info = "ID is not valid";

        return info;
    }

    public static void getAllCategory() {
        ArrayList<String> categoryList = new ArrayList<>();

        for (Task task : todolist) {
            if (!categoryList.contains(task.getCategory())) {
                categoryList.add(task.getCategory());
            }
        }

        for (int i = 0; i < categoryList.size(); i++) {
            System.out.println((i + 1) + ". " + categoryList.get(i));
        }
    }

    public static String getToDoList() {
        StringBuilder tempList = new StringBuilder();
        String tempListString = "";

        if (!todolist.isEmpty()) {
            for (int i = 0; i < todolist.size(); i++) {
                if (i < todolist.size() - 1) {
                    tempList.append(todolist.get(i).getTaskData()).append("\n");
                } else {
                    tempList.append(todolist.get(i).getTaskData());
                }
            }
            tempListString = tempList.toString();
        } else {
            tempListString = "To Do List is Empty";
        }

        return tempListString;
    }

    public static void getToDoListByCategory(String category) {
        ArrayList<Task> todolistbyCategory = new ArrayList<>();

        for (Task task : todolist) {
            if (task.isCategory(category)) {
                todolistbyCategory.add(task);
            }
        }

        for (Task task : todolistbyCategory) {
            System.out.println(task.getId() + ". " +
                    task.getName() + " [" +
                    task.getStatus() + "]");
        }
    }

    public static void insertToDoList(int id, String name, String status, String category) {
        Task task = new Task();

        task.insertTaskData(id, name, status, category);
        todolist.add(task);
    }

    public static String makeDoneToDoList(Task toDoList) {
        String info = "";

        if (toDoList != null) {
            if (toDoList.getStatus().equals("NOT DONE")) {
                toDoList.setStatus("DONE");
                info = "Task updated";
            } else if (toDoList.getStatus().equals("DONE")) {
                info = "Task already done!";
            }
        } else info = "ID is not valid";

        return info;
    }

    public static Task searchToDoList(int numSearch) {
        for (Task toDoList : todolist) {
            if (toDoList.getId() == numSearch) {
                return toDoList;
            }
        }
        return null;
    }
}
