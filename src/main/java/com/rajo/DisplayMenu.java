package com.rajo;

import java.util.Scanner;

public class DisplayMenu {
    private static Scanner myScan = new Scanner(System.in);

    public static void showMenu() {
        System.out.println("1. Insert New to Do List");
        System.out.println("2. View All to Do List");
        System.out.println("3. View All to Do List from a Category");
        System.out.println("4. Mark Done a to Do List");
        System.out.println("5. Delete a to Do List");
        System.out.println("0. Exit");
    }

    public static void formList() {
        int id;
        String name;
        String status;
        String category;

        System.out.println("Insert a to Do List");
        do {
            System.out.println("ID:");
            id = myScan.nextInt();
            myScan.nextLine();
        } while (ToDoList.searchToDoList(id) != null);

        do {
            System.out.println("Name:");
            name = myScan.nextLine();
        } while (name.matches(".*\\d+.*"));

        do {
            System.out.println("Status:");
            status = myScan.nextLine();
        } while (!status.equals("DONE") && !status.equals("NOT DONE"));

        do {
            System.out.println("Category:");
            category = myScan.nextLine();
        } while (category.matches(".*\\d+.*"));

        ToDoList.insertToDoList(id, name, status, category);
    }

    public static void infoViewAllList() {
        System.out.println("All to Do List");
        System.out.println(ToDoList.getToDoList());
    }

    public static void infoViewCategory() {
        String category;

        System.out.println("List Category");
        ToDoList.getAllCategory();
        System.out.println("Which Category?");
        category = myScan.nextLine();
        ToDoList.getToDoListByCategory(category);
    }

    public static void infoMarkDone() {
        System.out.println("Mark Done a to Do List");
        System.out.println(ToDoList.getToDoList());
        System.out.println("Which number?");
    }

    public static void infoDeleteList() {
        System.out.println("Delete a to Do List");
        System.out.println(ToDoList.getToDoList());
        System.out.println("Which number?");
    }

    public static void printSpacing(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println();
        }
    }
}
