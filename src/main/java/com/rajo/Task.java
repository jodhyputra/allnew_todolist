package com.rajo;

public class Task {
    private int id;
    private String name;
    private String status;
    private String category;

    public void insertTaskData(int id, String name, String status, String category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getCategory() {
        return category;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public boolean isCategory(String category){
        return  this.category == category;
    }

    public String getTaskData() {
        if (getName() == null) {
            return null;
        }
        return getId() + ". " + getName() + " " + "[" + getStatus() + "]" + " -> " + getCategory();
    }
}